package com.streams.eda.samples.order.core;

import com.streams.eda.samples.order.dto.OrderDto;
import com.streams.samples.OrderItem;
import com.streams.samples.OrderKey;
import com.streams.samples.OrderPlaced;
import com.streams.samples.OrderWorkflowStepCompleted;
import io.vavr.control.Either;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class OrderService {
    @Autowired
    private KafkaTemplate kafkaTemplate;

    @Transactional(transactionManager = "kafkaTransactionManager")
    public Either<Exception, OrderKey> placeOrder(@NonNull UUID orderId, @NonNull OrderDto order) {
        try {
            var orderPlaced = toOrderPlacedEvent(order);
            var orderKey = new OrderKey(orderId.toString());

            kafkaTemplate.send("order-placed", orderKey, orderPlaced);
            kafkaTemplate.send("order-workflow-step-completed", orderKey, createWorkflowEvent(order))
                    .addCallback(s -> log.info("Order with ID {} has been placed", orderKey.getId()),
                            f -> log.error("Failed to publish", f.getCause()));

            return Either.right(orderKey);
        } catch (Exception e) {
            log.error("Failed to publish order", e);
            return Either.left(e);
        }
    }

    private OrderWorkflowStepCompleted createWorkflowEvent(OrderDto order) {
        return new OrderWorkflowStepCompleted("order-placed", null, Instant.now().toEpochMilli());
    }

    private OrderPlaced toOrderPlacedEvent(OrderDto order) {
        var resultBuilder = OrderPlaced.newBuilder()
                .setOrderTime(Instant.now().toEpochMilli())
                .setUserId(order.getSalesPersonId())
                .setShopId(order.getShopId())
                .setTotalPaidAmount(order.getTotalAmount().toString());

        var orderItems = order.getItems().stream()
                .map(i -> new OrderItem(i.getItemId(), i.getQuantity()))
                .collect(Collectors.toList());
        resultBuilder.setOrderItems(orderItems);

        return resultBuilder.build();
    }
}
