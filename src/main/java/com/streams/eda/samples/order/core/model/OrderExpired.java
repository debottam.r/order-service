package com.streams.eda.samples.order.core.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Entity
@Table(name = "order-expired")
@Getter
@Setter
public class OrderExpired {
    @Id
    @Column(name = "key")
    private String id;

    @Column(name = "expiry", updatable = false, insertable = false)
    private long expiryTime;

    @OneToOne(fetch = FetchType.LAZY,optional = true)
    @JoinColumn(name = "key", nullable = true)
    private OrderPlaced orderPlaced;

    public LocalDateTime getExpiryTime() {
        return Instant.ofEpochMilli(expiryTime).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
}
