package com.streams.eda.samples.order.core.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Entity
@Table(name = "order-completed")
@Getter
@Setter
public class OrderCompleted {
    @Id
    @Column(name = "key")
    private String id;

    @Column(name = "completedTime", updatable = false, insertable = false)
    private long completedTime;

    @OneToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "key", nullable = true)
    private OrderPlaced orderPlaced;

    public LocalDateTime getCompletedTime() {
        return Instant.ofEpochMilli(completedTime).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
}
