package com.streams.eda.samples.order.core.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@Entity
@Table(name = "order-placed")
@Getter
@Setter
public class OrderPlaced {
    @Id
    @Column(name = "key", nullable = false, updatable = false, insertable = false)
    private String id;
    @Column(name = "totalPaidAmount", updatable = false, insertable = false)
    private String totalPaidAmount;
    @Column(name = "orderTime", updatable = false, insertable = false)
    private long orderTime;
    @Column(name = "shopId", updatable = false, insertable = false)
    private String shopId;
    @Column(name = "userId", updatable = false, insertable = false)
    private String userId;

    @OneToOne(fetch = FetchType.EAGER, mappedBy="orderPlaced", optional = true)
    private OrderCompleted orderCompleted;

    @OneToOne(fetch = FetchType.EAGER, mappedBy="orderPlaced", optional = true)
    private OrderExpired orderExpired;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "key")
    List<OrderWorkflowStepCompleted> orderWorkflowSteps;

    public LocalDateTime getOrderTime() {
        return Instant.ofEpochMilli(orderTime).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
}
