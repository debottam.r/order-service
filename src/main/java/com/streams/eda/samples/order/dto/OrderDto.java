package com.streams.eda.samples.order.dto;

import lombok.Value;

import java.math.BigDecimal;
import java.util.List;

@Value
public class OrderDto {
    private final String salesPersonId;
    private final String shopId;
    private final BigDecimal totalAmount;
    private final List<Item> items;

    @Value
    public static class Item {
        private final String itemId;
        private final double quantity;
    }
}
